//	Greenplum Database
//	Copyright (C) 2016 Pivotal Software, Inc.

#ifndef SPQOPT_CPhysicalParallelUnionAllTest_H
#define SPQOPT_CPhysicalParallelUnionAllTest_H

#include "spqos/types.h"

namespace spqopt
{
class CPhysicalParallelUnionAllTest
{
public:
	static spqos::SPQOS_RESULT EresUnittest();
};
}  // namespace spqopt

#endif
