//	Greenplum Database
//	Copyright (C) 2016 Pivotal Software, Inc.

#ifndef SPQOPT_CPullUpProjectElementTest_H
#define SPQOPT_CPullUpProjectElementTest_H

#include "spqos/base.h"

namespace spqopt
{
class CPullUpProjectElementTest
{
public:
	static spqos::SPQOS_RESULT EresUnittest();
};
}  // namespace spqopt

#endif	//SPQOPT_CPullUpProjectElementTest_H
