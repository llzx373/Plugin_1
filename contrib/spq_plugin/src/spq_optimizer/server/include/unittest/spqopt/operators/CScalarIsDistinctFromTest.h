#ifndef SPQOPT_CScalarIsDistinctFromTest_H
#define SPQOPT_CScalarIsDistinctFromTest_H

#include "spqos/types.h"

namespace spqopt
{
class CScalarIsDistinctFromTest
{
public:
	static spqos::SPQOS_RESULT EresUnittest();
};
}  // namespace spqopt
#endif	// SPQOPT_CScalarIsDistinctFromTest_H
