//---------------------------------------------------------------------------
//	Greenplum Database
//	Copyright (C) 2011 EMC Corp.
//
//	@filename:
//		COptimizationContext.cpp
//
//	@doc:
//		Implementation of optimization context
//---------------------------------------------------------------------------

#include "spqopt/base/COptimizationContext.h"

#include "spqos/base.h"
#include "spqos/error/CAutoTrace.h"

#include "spqopt/base/CEnfdOrder.h"
#include "spqopt/base/COrderSpec.h"
#include "spqopt/operators/CPhysicalAgg.h"
#include "spqopt/operators/CPhysicalCTEProducer.h"
#include "spqopt/operators/CPhysicalMotion.h"
#include "spqopt/operators/CPhysicalNLJoin.h"
#include "spqopt/operators/CPhysicalSort.h"
#include "spqopt/search/CGroupExpression.h"
#include "spqopt/xforms/CXformUtils.h"


using namespace spqopt;

FORCE_GENERATE_DBGSTR(COptimizationContext);

// invalid optimization context
const COptimizationContext COptimizationContext::m_ocInvalid;

// invalid optimization context pointer
const OPTCTXT_PTR COptimizationContext::m_pocInvalid = NULL;



//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::~COptimizationContext
//
//	@doc:
//		Dtor
//
//---------------------------------------------------------------------------
COptimizationContext::~COptimizationContext()
{
	CRefCount::SafeRelease(m_prpp);
	CRefCount::SafeRelease(m_prprel);
	CRefCount::SafeRelease(m_pdrspqstatCtxt);
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::PgexprBest
//
//	@doc:
//		Best group expression accessor
//
//---------------------------------------------------------------------------
CGroupExpression *
COptimizationContext::PgexprBest() const
{
	if (NULL == m_pccBest)
	{
		return NULL;
	}

	return m_pccBest->Pgexpr();
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::SetBest
//
//	@doc:
//		 Set best cost context
//
//---------------------------------------------------------------------------
void
COptimizationContext::SetBest(CCostContext *pcc)
{
	SPQOS_ASSERT(NULL != pcc);

	m_pccBest = pcc;

	COperator *pop = pcc->Pgexpr()->Pop();
	if (CUtils::FPhysicalAgg(pop) &&
		CPhysicalAgg::PopConvert(pop)->FMultiStage())
	{
		m_fHasMultiStageAggPlan = true;
	}
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::Matches
//
//	@doc:
//		Match against another context
//
//---------------------------------------------------------------------------
BOOL
COptimizationContext::Matches(const COptimizationContext *poc) const
{
	SPQOS_ASSERT(NULL != poc);

	if (m_pgroup != poc->Pgroup() ||
		m_ulSearchStageIndex != poc->UlSearchStageIndex())
	{
		return false;
	}

	CReqdPropPlan *prppFst = this->Prpp();
	CReqdPropPlan *prppSnd = poc->Prpp();

	// make sure we are not comparing to invalid context
	if (NULL == prppFst || NULL == prppSnd)
	{
		return NULL == prppFst && NULL == prppSnd;
	}

	return prppFst->Equals(prppSnd);
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::FEqualForStats
//
//	@doc:
//		Equality function used for computing stats during costing
//
//---------------------------------------------------------------------------
BOOL
COptimizationContext::FEqualForStats(const COptimizationContext *pocLeft,
									 const COptimizationContext *pocRight)
{
	SPQOS_ASSERT(NULL != pocLeft);
	SPQOS_ASSERT(NULL != pocRight);

	return pocLeft->GetReqdRelationalProps()->PcrsStat()->Equals(
			   pocRight->GetReqdRelationalProps()->PcrsStat()) &&
		   pocLeft->Pdrspqstat()->Equals(pocRight->Pdrspqstat()) &&
		   pocLeft->Prpp()->Pepp()->PpfmDerived()->Equals(
			   pocRight->Prpp()->Pepp()->PpfmDerived());
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::FOptimize
//
//	@doc:
//		Return true if given group expression should be optimized under
//		given context
//
//---------------------------------------------------------------------------
BOOL
COptimizationContext::FOptimize(CMemoryPool *mp, CGroupExpression *pgexprParent,
								CGroupExpression *pgexprChild,
								COptimizationContext *pocChild,
								ULONG ulSearchStages)
{
	COperator *pop = pgexprChild->Pop();

	if (CUtils::FPhysicalMotion(pop))
	{
		return FOptimizeMotion(mp, pgexprParent, pgexprChild, pocChild,
							   ulSearchStages);
	}

	if (COperator::EopPhysicalSort == pop->Eopid())
	{
		return FOptimizeSort(mp, pgexprParent, pgexprChild, pocChild,
							 ulSearchStages);
	}

	if (CUtils::FPhysicalAgg(pop))
	{
		return FOptimizeAgg(mp, pgexprParent, pgexprChild, pocChild,
							ulSearchStages);
	}

	if (CUtils::FNLJoin(pop))
	{
		return FOptimizeNLJoin(mp, pgexprParent, pgexprChild, pocChild,
							   ulSearchStages);
	}

	return true;
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::FEqualIds
//
//	@doc:
//		Compare array of optimization contexts based on context ids
//
//---------------------------------------------------------------------------
BOOL
COptimizationContext::FEqualContextIds(COptimizationContextArray *pdrspqocFst,
									   COptimizationContextArray *pdrspqocSnd)
{
	if (NULL == pdrspqocFst || NULL == pdrspqocSnd)
	{
		return (NULL == pdrspqocFst && NULL == pdrspqocSnd);
	}

	const ULONG ulCtxts = pdrspqocFst->Size();
	if (ulCtxts != pdrspqocSnd->Size())
	{
		return false;
	}

	BOOL fEqual = true;
	for (ULONG ul = 0; fEqual && ul < ulCtxts; ul++)
	{
		fEqual = (*pdrspqocFst)[ul]->Id() == (*pdrspqocSnd)[ul]->Id();
	}

	return fEqual;
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::FOptimizeMotion
//
//	@doc:
//		Check if a Motion node should be optimized for the given context
//
//---------------------------------------------------------------------------
BOOL
COptimizationContext::FOptimizeMotion(CMemoryPool *,	   // mp
									  CGroupExpression *,  // pgexprParent
									  CGroupExpression *pgexprMotion,
									  COptimizationContext *poc,
									  ULONG	 // ulSearchStages
)
{
	SPQOS_ASSERT(NULL != pgexprMotion);
	SPQOS_ASSERT(NULL != poc);
	SPQOS_ASSERT(CUtils::FPhysicalMotion(pgexprMotion->Pop()));

	CPhysicalMotion *pop = CPhysicalMotion::PopConvert(pgexprMotion->Pop());

	return poc->Prpp()->Ped()->FCompatible(pop->Pds());
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::FOptimizeSort
//
//	@doc:
//		Check if a Sort node should be optimized for the given context
//
//---------------------------------------------------------------------------
BOOL
COptimizationContext::FOptimizeSort(CMemoryPool *,		 // mp
									CGroupExpression *,	 // pgexprParent
									CGroupExpression *pgexprSort,
									COptimizationContext *poc,
									ULONG  // ulSearchStages
)
{
	SPQOS_ASSERT(NULL != pgexprSort);
	SPQOS_ASSERT(NULL != poc);
	SPQOS_ASSERT(COperator::EopPhysicalSort == pgexprSort->Pop()->Eopid());

	CPhysicalSort *pop = CPhysicalSort::PopConvert(pgexprSort->Pop());

	return poc->Prpp()->Peo()->FCompatible(
		const_cast<COrderSpec *>(pop->Pos()));
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::FOptimizeAgg
//
//	@doc:
//		Check if Agg node should be optimized for the given context
//
//---------------------------------------------------------------------------
BOOL
COptimizationContext::FOptimizeAgg(CMemoryPool *mp,
								   CGroupExpression *,	// pgexprParent
								   CGroupExpression *pgexprAgg,
								   COptimizationContext *poc,
								   ULONG ulSearchStages)
{
	SPQOS_ASSERT(NULL != pgexprAgg);
	SPQOS_ASSERT(NULL != poc);
	SPQOS_ASSERT(CUtils::FPhysicalAgg(pgexprAgg->Pop()));
	SPQOS_ASSERT(0 < ulSearchStages);

	if (SPQOS_FTRACE(EopttraceForceExpandedMDQAs))
	{
		BOOL fHasMultipleDistinctAggs =
			CDrvdPropScalar::GetDrvdScalarProps((*pgexprAgg)[1]->Pdp())
				->HasMultipleDistinctAggs();
		if (fHasMultipleDistinctAggs)
		{
			// do not optimize plans with MDQAs, since preference is for plans with expanded MDQAs
			return false;
		}
	}

	if (!SPQOS_FTRACE(EopttraceForceMultiStageAgg))
	{
		// no preference for multi-stage agg, we always proceed with optimization
		return true;
	}

	// otherwise, we need to avoid optimizing node unless it is a multi-stage agg
	COptimizationContext *pocFound =
		pgexprAgg->Pgroup()->PocLookupBest(mp, ulSearchStages, poc->Prpp());
	if (NULL != pocFound && pocFound->FHasMultiStageAggPlan())
	{
		// context already has a multi-stage agg plan, optimize child only if it is also a multi-stage agg
		return CPhysicalAgg::PopConvert(pgexprAgg->Pop())->FMultiStage();
	}

	// child context has no plan yet, return true
	return true;
}


//---------------------------------------------------------------------------
//	@function:
//		CPhysicalNLJoin::FOptimizeNLJoin
//
//	@doc:
//		Check if NL join node should be optimized for the given context
//
//---------------------------------------------------------------------------
BOOL
COptimizationContext::FOptimizeNLJoin(CMemoryPool *mp,
									  CGroupExpression *,  // pgexprParent
									  CGroupExpression *pgexprJoin,
									  COptimizationContext *poc,
									  ULONG	 // ulSearchStages
)
{
	SPQOS_ASSERT(NULL != pgexprJoin);
	SPQOS_ASSERT(NULL != poc);
	SPQOS_ASSERT(CUtils::FNLJoin(pgexprJoin->Pop()));

	COperator *pop = pgexprJoin->Pop();
	if (!CUtils::FCorrelatedNLJoin(pop))
	{
		return true;
	}

	// for correlated join, the requested columns must be covered by outer child
	// columns and columns to be generated from inner child
	CPhysicalNLJoin *popNLJoin = CPhysicalNLJoin::PopConvert(pop);
	CColRefSet *pcrs = SPQOS_NEW(mp) CColRefSet(mp, popNLJoin->PdrgPcrInner());
	CColRefSet *pcrsOuterChild =
		CDrvdPropRelational::GetRelationalProperties((*pgexprJoin)[0]->Pdp())
			->GetOutputColumns();
	pcrs->Include(pcrsOuterChild);
	BOOL fIncluded = pcrs->ContainsAll(poc->Prpp()->PcrsRequired());
	pcrs->Release();

	return fIncluded;
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::PrppCTEProducer
//
//	@doc:
//		Compute required properties to CTE producer based on plan properties
//		of CTE consumer
//
//---------------------------------------------------------------------------
CReqdPropPlan *
COptimizationContext::PrppCTEProducer(CMemoryPool *mp,
									  COptimizationContext *poc,
									  ULONG ulSearchStages)
{
	SPQOS_ASSERT(NULL != poc);
	SPQOS_ASSERT(NULL != poc->PccBest());

	CCostContext *pccBest = poc->PccBest();
	CGroupExpression *pgexpr = pccBest->Pgexpr();
	BOOL fOptimizeCTESequence =
		(COperator::EopPhysicalSequence == pgexpr->Pop()->Eopid() &&
		 (*pgexpr)[0]->FHasCTEProducer());

	if (!fOptimizeCTESequence)
	{
		// best group expression is not a CTE sequence
		return NULL;
	}

	COptimizationContext *pocProducer = (*pgexpr)[0]->PocLookupBest(
		mp, ulSearchStages, (*pccBest->Pdrspqoc())[0]->Prpp());
	if (NULL == pocProducer)
	{
		return NULL;
	}

	CCostContext *pccProducer = pocProducer->PccBest();
	if (NULL == pccProducer)
	{
		return NULL;
	}
	COptimizationContext *pocConsumer = (*pgexpr)[1]->PocLookupBest(
		mp, ulSearchStages, (*pccBest->Pdrspqoc())[1]->Prpp());
	if (NULL == pocConsumer)
	{
		return NULL;
	}

	CCostContext *pccConsumer = pocConsumer->PccBest();
	if (NULL == pccConsumer)
	{
		return NULL;
	}

	CColRefSet *pcrsInnerOutput =
		CDrvdPropRelational::GetRelationalProperties((*pgexpr)[1]->Pdp())
			->GetOutputColumns();
	CPhysicalCTEProducer *popProducer =
		CPhysicalCTEProducer::PopConvert(pccProducer->Pgexpr()->Pop());
	UlongToColRefMap *colref_mapping =
		COptCtxt::PoctxtFromTLS()->Pcteinfo()->PhmulcrConsumerToProducer(
			mp, popProducer->UlCTEId(), pcrsInnerOutput,
			popProducer->Pdrspqcr());
	CReqdPropPlan *prppProducer = CReqdPropPlan::PrppRemapForCTE(
		mp, pocProducer->Prpp(), pccConsumer->Pdpplan(), colref_mapping);
	colref_mapping->Release();

	if (prppProducer->Equals(pocProducer->Prpp()))
	{
		prppProducer->Release();

		return NULL;
	}

	return prppProducer;
}


//---------------------------------------------------------------------------
//	@function:
//		COptimizationContext::OsPrint
//
//	@doc:
//		Debug print
//
//---------------------------------------------------------------------------
IOstream &
COptimizationContext::OsPrint(IOstream &os) const
{
	return OsPrintWithPrefix(os, "");
}

IOstream &
COptimizationContext::OsPrintWithPrefix(IOstream &os,
										const CHAR *szPrefix) const
{
	os << szPrefix << m_id << " (stage " << m_ulSearchStageIndex << "): ("
	   << *m_prpp << ") => Best Expr:";
	if (NULL != PgexprBest())
	{
		os << PgexprBest()->Id();
	}
	os << std::endl;

	return os;
}

// EOF
