//---------------------------------------------------------------------------
//	Greenplum Database
//	Copyright (C) 2008 Greenplum, Inc.
//
//	@filename:
//		base.h
//
//	@doc:
//		Collection of commonly used OS abstraction primitives;
//		Most files should be fine including only this one from the SPQOS folder;
//---------------------------------------------------------------------------
#ifndef SPQOS_base_H
#define SPQOS_base_H

#include "spqos/assert.h"
#include "spqos/error/CErrorHandler.h"
#include "spqos/error/CException.h"
#include "spqos/error/IErrorContext.h"
#include "spqos/error/ILogger.h"
#include "spqos/memory/CMemoryPool.h"
#include "spqos/types.h"

#endif	// SPQOS_base_H

// EOF
