create schema test_supplemental_operator;
set search_path = 'test_supplemental_operator';
set dolphin.b_compatibility_mode = on;
create table t_boolean(col1 boolean);
create table t_year(col1 year);
create table t_binary(col1 binary(100));
create table t_varbinary(col1 varbinary(100));
create table t_tinyblob(col1 tinyblob);
create table t_blob(col1 blob);
create table t_mediumblob(col1 mediumblob);
create table t_longblob(col1 longblob);

insert into t_boolean values(1),(0);
insert into t_binary values('1.2'),('1'),('0.2'),('0');
insert into t_varbinary values('1.2'),('1'),('0.2'),('0');
insert into t_tinyblob values('1.2'),('1'),('0.2'),('0');
insert into t_blob values('1.2'),('1'),('0.2'),('0');
insert into t_mediumblob values('1.2'),('1'),('0.2'),('0');
insert into t_longblob values('1.2'),('1'),('0.2'),('0');

select * from t_boolean inner join t_binary where t_boolean.col1 = t_binary.col1;
select * from t_boolean inner join t_binary where t_binary.col1 = t_boolean.col1;
select * from t_boolean inner join t_binary where t_boolean.col1 != t_binary.col1;
select * from t_boolean inner join t_binary where t_binary.col1 != t_boolean.col1;
select * from t_boolean inner join t_binary where t_boolean.col1 >= t_binary.col1;
select * from t_boolean inner join t_binary where t_binary.col1 >= t_boolean.col1;
select * from t_boolean inner join t_binary where t_boolean.col1 <= t_binary.col1;
select * from t_boolean inner join t_binary where t_binary.col1 <= t_boolean.col1;
select * from t_boolean inner join t_binary where t_boolean.col1 > t_binary.col1;
select * from t_boolean inner join t_binary where t_binary.col1 > t_boolean.col1;
select * from t_boolean inner join t_binary where t_boolean.col1 < t_binary.col1;
select * from t_boolean inner join t_binary where t_binary.col1 < t_boolean.col1;

select * from t_varbinary inner join t_boolean where t_varbinary.col1 = t_boolean.col1;
select * from t_varbinary inner join t_boolean where t_boolean.col1 = t_varbinary.col1;
select * from t_varbinary inner join t_boolean where t_varbinary.col1 != t_boolean.col1;
select * from t_varbinary inner join t_boolean where t_boolean.col1 != t_varbinary.col1;
select * from t_varbinary inner join t_boolean where t_varbinary.col1 >= t_boolean.col1;
select * from t_varbinary inner join t_boolean where t_boolean.col1 >= t_varbinary.col1;
select * from t_varbinary inner join t_boolean where t_varbinary.col1 <= t_boolean.col1;
select * from t_varbinary inner join t_boolean where t_boolean.col1 <= t_varbinary.col1;
select * from t_varbinary inner join t_boolean where t_varbinary.col1 > t_boolean.col1;
select * from t_varbinary inner join t_boolean where t_boolean.col1 > t_varbinary.col1;
select * from t_varbinary inner join t_boolean where t_varbinary.col1 < t_boolean.col1;
select * from t_varbinary inner join t_boolean where t_boolean.col1 < t_varbinary.col1;

select * from t_tinyblob inner join t_boolean where t_tinyblob.col1 = t_boolean.col1;
select * from t_tinyblob inner join t_boolean where t_boolean.col1 = t_tinyblob.col1;
select * from t_tinyblob inner join t_boolean where t_tinyblob.col1 != t_boolean.col1;
select * from t_tinyblob inner join t_boolean where t_boolean.col1 != t_tinyblob.col1;
select * from t_tinyblob inner join t_boolean where t_tinyblob.col1 >= t_boolean.col1;
select * from t_tinyblob inner join t_boolean where t_boolean.col1 >= t_tinyblob.col1;
select * from t_tinyblob inner join t_boolean where t_tinyblob.col1 <= t_boolean.col1;
select * from t_tinyblob inner join t_boolean where t_boolean.col1 <= t_tinyblob.col1;
select * from t_tinyblob inner join t_boolean where t_tinyblob.col1 > t_boolean.col1;
select * from t_tinyblob inner join t_boolean where t_boolean.col1 > t_tinyblob.col1;
select * from t_tinyblob inner join t_boolean where t_tinyblob.col1 < t_boolean.col1;
select * from t_tinyblob inner join t_boolean where t_boolean.col1 < t_tinyblob.col1;

select * from t_blob inner join t_boolean where t_blob.col1 = t_boolean.col1;
select * from t_blob inner join t_boolean where t_boolean.col1 = t_blob.col1;
select * from t_blob inner join t_boolean where t_blob.col1 != t_boolean.col1;
select * from t_blob inner join t_boolean where t_boolean.col1 != t_blob.col1;
select * from t_blob inner join t_boolean where t_blob.col1 >= t_boolean.col1;
select * from t_blob inner join t_boolean where t_boolean.col1 >= t_blob.col1;
select * from t_blob inner join t_boolean where t_blob.col1 <= t_boolean.col1;
select * from t_blob inner join t_boolean where t_boolean.col1 <= t_blob.col1;
select * from t_blob inner join t_boolean where t_blob.col1 > t_boolean.col1;
select * from t_blob inner join t_boolean where t_boolean.col1 > t_blob.col1;
select * from t_blob inner join t_boolean where t_blob.col1 < t_boolean.col1;
select * from t_blob inner join t_boolean where t_boolean.col1 < t_blob.col1;

select * from t_mediumblob inner join t_boolean where t_mediumblob.col1 = t_boolean.col1;
select * from t_mediumblob inner join t_boolean where t_boolean.col1 = t_mediumblob.col1;
select * from t_mediumblob inner join t_boolean where t_mediumblob.col1 != t_boolean.col1;
select * from t_mediumblob inner join t_boolean where t_boolean.col1 != t_mediumblob.col1;
select * from t_mediumblob inner join t_boolean where t_mediumblob.col1 >= t_boolean.col1;
select * from t_mediumblob inner join t_boolean where t_boolean.col1 >= t_mediumblob.col1;
select * from t_mediumblob inner join t_boolean where t_mediumblob.col1 <= t_boolean.col1;
select * from t_mediumblob inner join t_boolean where t_boolean.col1 <= t_mediumblob.col1;
select * from t_mediumblob inner join t_boolean where t_mediumblob.col1 > t_boolean.col1;
select * from t_mediumblob inner join t_boolean where t_boolean.col1 > t_mediumblob.col1;
select * from t_mediumblob inner join t_boolean where t_mediumblob.col1 < t_boolean.col1;
select * from t_mediumblob inner join t_boolean where t_boolean.col1 < t_mediumblob.col1;

select * from t_longblob inner join t_boolean where t_longblob.col1 = t_boolean.col1;
select * from t_longblob inner join t_boolean where t_boolean.col1 = t_longblob.col1;
select * from t_longblob inner join t_boolean where t_longblob.col1 != t_boolean.col1;
select * from t_longblob inner join t_boolean where t_boolean.col1 != t_longblob.col1;
select * from t_longblob inner join t_boolean where t_longblob.col1 >= t_boolean.col1;
select * from t_longblob inner join t_boolean where t_boolean.col1 >= t_longblob.col1;
select * from t_longblob inner join t_boolean where t_longblob.col1 <= t_boolean.col1;
select * from t_longblob inner join t_boolean where t_boolean.col1 <= t_longblob.col1;
select * from t_longblob inner join t_boolean where t_longblob.col1 > t_boolean.col1;
select * from t_longblob inner join t_boolean where t_boolean.col1 > t_longblob.col1;
select * from t_longblob inner join t_boolean where t_longblob.col1 < t_boolean.col1;
select * from t_longblob inner join t_boolean where t_boolean.col1 < t_longblob.col1;

delete from t_binary;
delete from t_varbinary;
delete from t_tinyblob;
delete from t_blob;
delete from t_mediumblob;
delete from t_longblob;

insert into t_year values(2023);
insert into t_binary values('2023'),('2022'),('2024');
insert into t_varbinary values('2023'),('2022'),('2024');
insert into t_tinyblob values('2023'),('2022'),('2024');
insert into t_blob values('2023'),('2022'),('2024');
insert into t_mediumblob values('2023'),('2022'),('2024');
insert into t_longblob values('2023'),('2022'),('2024');

select * from t_year inner join t_binary where t_year.col1 = t_binary.col1;
select * from t_year inner join t_binary where t_binary.col1 = t_year.col1;
select * from t_year inner join t_binary where t_year.col1 != t_binary.col1;
select * from t_year inner join t_binary where t_binary.col1 != t_year.col1;
select * from t_year inner join t_binary where t_year.col1 >= t_binary.col1;
select * from t_year inner join t_binary where t_binary.col1 >= t_year.col1;
select * from t_year inner join t_binary where t_year.col1 <= t_binary.col1;
select * from t_year inner join t_binary where t_binary.col1 <= t_year.col1;
select * from t_year inner join t_binary where t_year.col1 > t_binary.col1;
select * from t_year inner join t_binary where t_binary.col1 > t_year.col1;
select * from t_year inner join t_binary where t_year.col1 < t_binary.col1;
select * from t_year inner join t_binary where t_binary.col1 < t_year.col1;

select * from t_varbinary inner join t_year where t_varbinary.col1 = t_year.col1;
select * from t_varbinary inner join t_year where t_year.col1 = t_varbinary.col1;
select * from t_varbinary inner join t_year where t_varbinary.col1 != t_year.col1;
select * from t_varbinary inner join t_year where t_year.col1 != t_varbinary.col1;
select * from t_varbinary inner join t_year where t_varbinary.col1 >= t_year.col1;
select * from t_varbinary inner join t_year where t_year.col1 >= t_varbinary.col1;
select * from t_varbinary inner join t_year where t_varbinary.col1 <= t_year.col1;
select * from t_varbinary inner join t_year where t_year.col1 <= t_varbinary.col1;
select * from t_varbinary inner join t_year where t_varbinary.col1 > t_year.col1;
select * from t_varbinary inner join t_year where t_year.col1 > t_varbinary.col1;
select * from t_varbinary inner join t_year where t_varbinary.col1 < t_year.col1;
select * from t_varbinary inner join t_year where t_year.col1 < t_varbinary.col1;

select * from t_tinyblob inner join t_year where t_tinyblob.col1 = t_year.col1;
select * from t_tinyblob inner join t_year where t_year.col1 = t_tinyblob.col1;
select * from t_tinyblob inner join t_year where t_tinyblob.col1 != t_year.col1;
select * from t_tinyblob inner join t_year where t_year.col1 != t_tinyblob.col1;
select * from t_tinyblob inner join t_year where t_tinyblob.col1 >= t_year.col1;
select * from t_tinyblob inner join t_year where t_year.col1 >= t_tinyblob.col1;
select * from t_tinyblob inner join t_year where t_tinyblob.col1 <= t_year.col1;
select * from t_tinyblob inner join t_year where t_year.col1 <= t_tinyblob.col1;
select * from t_tinyblob inner join t_year where t_tinyblob.col1 > t_year.col1;
select * from t_tinyblob inner join t_year where t_year.col1 > t_tinyblob.col1;
select * from t_tinyblob inner join t_year where t_tinyblob.col1 < t_year.col1;
select * from t_tinyblob inner join t_year where t_year.col1 < t_tinyblob.col1;

select * from t_blob inner join t_year where t_blob.col1 = t_year.col1;
select * from t_blob inner join t_year where t_year.col1 = t_blob.col1;
select * from t_blob inner join t_year where t_blob.col1 != t_year.col1;
select * from t_blob inner join t_year where t_year.col1 != t_blob.col1;
select * from t_blob inner join t_year where t_blob.col1 >= t_year.col1;
select * from t_blob inner join t_year where t_year.col1 >= t_blob.col1;
select * from t_blob inner join t_year where t_blob.col1 <= t_year.col1;
select * from t_blob inner join t_year where t_year.col1 <= t_blob.col1;
select * from t_blob inner join t_year where t_blob.col1 > t_year.col1;
select * from t_blob inner join t_year where t_year.col1 > t_blob.col1;
select * from t_blob inner join t_year where t_blob.col1 < t_year.col1;
select * from t_blob inner join t_year where t_year.col1 < t_blob.col1;

select * from t_mediumblob inner join t_year where t_mediumblob.col1 = t_year.col1;
select * from t_mediumblob inner join t_year where t_year.col1 = t_mediumblob.col1;
select * from t_mediumblob inner join t_year where t_mediumblob.col1 != t_year.col1;
select * from t_mediumblob inner join t_year where t_year.col1 != t_mediumblob.col1;
select * from t_mediumblob inner join t_year where t_mediumblob.col1 >= t_year.col1;
select * from t_mediumblob inner join t_year where t_year.col1 >= t_mediumblob.col1;
select * from t_mediumblob inner join t_year where t_mediumblob.col1 <= t_year.col1;
select * from t_mediumblob inner join t_year where t_year.col1 <= t_mediumblob.col1;
select * from t_mediumblob inner join t_year where t_mediumblob.col1 > t_year.col1;
select * from t_mediumblob inner join t_year where t_year.col1 > t_mediumblob.col1;
select * from t_mediumblob inner join t_year where t_mediumblob.col1 < t_year.col1;
select * from t_mediumblob inner join t_year where t_year.col1 < t_mediumblob.col1;

select * from t_longblob inner join t_year where t_longblob.col1 = t_year.col1;
select * from t_longblob inner join t_year where t_year.col1 = t_longblob.col1;
select * from t_longblob inner join t_year where t_longblob.col1 != t_year.col1;
select * from t_longblob inner join t_year where t_year.col1 != t_longblob.col1;
select * from t_longblob inner join t_year where t_longblob.col1 >= t_year.col1;
select * from t_longblob inner join t_year where t_year.col1 >= t_longblob.col1;
select * from t_longblob inner join t_year where t_longblob.col1 <= t_year.col1;
select * from t_longblob inner join t_year where t_year.col1 <= t_longblob.col1;
select * from t_longblob inner join t_year where t_longblob.col1 > t_year.col1;
select * from t_longblob inner join t_year where t_year.col1 > t_longblob.col1;
select * from t_longblob inner join t_year where t_longblob.col1 < t_year.col1;
select * from t_longblob inner join t_year where t_year.col1 < t_longblob.col1;

reset search_path;
drop schema test_supplemental_operator cascade;
