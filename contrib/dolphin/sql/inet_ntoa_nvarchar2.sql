create schema inet_ntoa_nvarchar2;
set current_schema = inet_ntoa_nvarchar2;
set dolphin.sql_mode = 'sql_mode_strict,sql_mode_full_group,pipes_as_concat,no_zero_date,pad_char_to_full_length,auto_recompile_function,error_for_division_by_zero';
set dolphin.b_compatibility_mode=on;
set bytea_output=escape;
set extra_float_digits=0;
set b_format_behavior_compat_options='enable_set_variables';
create table t_text0001(
    c1 national varchar(1),
    c2 national varchar(10),
    c3 national varchar(255),
    c4 text,
    c5 text(1),
    c6 text(10),
    c7 text(255),
    c8 tinytext,
    c9 mediumtext,
    c10 longtext,
    c11 varchar(1),
    c12 varchar(255),
    c13 varchar(20000));
set @val = 3.14159265;
insert into t_text0001 values
(substr(@val, 1, 1), @val, @val, @val, substr(@val, 1, 1),
@val, @val, @val, @val, @val, substr(@val, 1, 1), @val, @val);
set @val = 0;
insert into t_text0001 values
(@val, @val, @val, @val, @val, @val, @val, @val, @val, @val, @val, @val, @val);
set @val = -1;
insert into t_text0001 values
(substr(@val, 1, 1), @val, @val, @val, substr(@val, 1, 1),
@val, @val, @val, @val, @val, substr(@val, 1, 1), @val, @val);
set @val = 'abcdefghigklmnopqrstuvw';
set @val1 = substr(@val, 1, 1);
set @val2 = substr(@val, 1, 2);
set @val3 = repeat(@val, 3);
insert into t_text0001 values
(@val1, @val1, @val1, @val1, @val1, @val1,
@val1, @val1, @val1, @val1, @val1, @val1, @val1);
insert into t_text0001 values
(substr(@val2, 1, 1), @val2, @val2, @val2, substr(@val2, 1, 1), @val2,
@val2, @val2, @val2, @val2, substr(@val2, 1, 1), @val2, @val2);
insert into t_text0001 values
(substr(@val3, 1, 1),substr(@val3, 1, 10), substr(@val3, 1, 255),
@val3, substr(@val3, 1, 1), substr(@val3, 1, 10), substr(@val3, 1, 255),
substr(@val3, 1, 133), @val3, @val3,
substr(@val3, 1, 1), @val3, repeat(@val3, 10));
select str_to_date(c1, "%Y-%m-%d"), str_to_date(c2, "%Y-%m-%d"), str_to_date(c3,
"%Y-%m-%d"), str_to_date(c4, "%Y-%m-%d"), str_to_date(c5, "%Y-%m-%d"),
str_to_date(c6, "%Y-%m-%d"), str_to_date(c7, "%Y-%m-%d"), str_to_date(c8,
"%Y-%m-%d"), str_to_date(c9, "%Y-%m-%d"), str_to_date(c10, "%Y-%m-%d"),
str_to_date(c11, "%Y-%m-%d"), str_to_date(c12, "%Y-%m-%d"), str_to_date(c13,
"%Y-%m-%d") from t_text0001 order by 1,2,3,4,5,6,7,8,9,10,11,12,13;

select inet_ntoa(c1), inet_ntoa(c2), inet_ntoa(c3), inet_ntoa(c4),
inet_ntoa(c5), inet_ntoa(c6), inet_ntoa(c7), inet_ntoa(c8), inet_ntoa(c9),
inet_ntoa(c10), inet_ntoa(c11), inet_ntoa(c12), inet_ntoa(c13) from t_text0001
order by 1,2,3,4,5,6,7,8,9,10,11,12,13;

CREATE TABLE test_type_table
(
    `int1` tinyint,
    `uint1` tinyint unsigned,
    `int2` smallint,
    `uint2` smallint unsigned,
    `int4` integer,
    `uint4` integer unsigned,
    `int8` bigint,
    `uint8` bigint unsigned,
    `float4` float4,
    `float8` float8,
    `numeric` decimal(20, 6),
    `bit1` bit(1),
    `bit64` bit(64),
    `boolean` boolean,
    `date` date,
    `time` time,
    `time(4)` time(4),
    `datetime` datetime,
    `datetime(4)` datetime(4) default '2022-11-11 11:11:11',
    `timestamp` timestamp,
    `timestamp(4)` timestamp(4) default '2022-11-11 11:11:11',
    `year` year,
    `char` char(100),
    `varchar` varchar(100),
    `binary` binary(100),
    `varbinary` varbinary(100),
    `tinyblob` tinyblob,
    `blob` blob,
    `mediumblob` mediumblob,
    `longblob` longblob,
    `text` text,
    `enum_t` enum('a', 'b', 'c'),
    `set_t` set('a', 'b', 'c'),
    `json` json
);

insert into test_type_table values(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,b'1', b'111', true,'2023-02-05', '19:10:50', '19:10:50.3456', '2023-02-05 19:10:50', '2023-02-05 19:10:50.456', '2023-02-05 19:10:50', '2023-02-05 19:10:50.456', '2023','1.23a', '1.23a', '1.23a', '1.23a', '1.23a', '1.23a', '1.23a', '1.23a', '1.23a','a', 'a,c',json_object('a', 1, 'b', 2));

select
inet_ntoa(`int1`),
inet_ntoa(`uint1`),
inet_ntoa(`int2`),
inet_ntoa(`uint2`),
inet_ntoa(`int4`),
inet_ntoa(`uint4`),
inet_ntoa(`int8`),
inet_ntoa(`uint8`),
inet_ntoa(`float4`),
inet_ntoa(`float8`),
inet_ntoa(`numeric`),
inet_ntoa(`bit1`),
inet_ntoa(`bit64`),
inet_ntoa(`boolean`),
inet_ntoa(`date`),
inet_ntoa(`time`),
inet_ntoa(`time(4)`),
inet_ntoa(`datetime`),
inet_ntoa(`datetime(4)`),
inet_ntoa(`timestamp`),
inet_ntoa(`timestamp(4)`),
inet_ntoa(`year`),
inet_ntoa(`char`),
inet_ntoa(`varchar`),
inet_ntoa(`binary`),
inet_ntoa(`varbinary`),
inet_ntoa(`tinyblob`),
inet_ntoa(`blob`),
inet_ntoa(`mediumblob`),
inet_ntoa(`longblob`),
inet_ntoa(`text`),
inet_ntoa(`enum_t`),
inet_ntoa(`set_t`),
inet_ntoa(`json`)
from test_type_table;

drop table test_type_table;
drop table t_text0001;
reset current_schema;
drop schema inet_ntoa_nvarchar2;
