create schema db_b_parser3;
set current_schema to 'db_b_parser3';

--测试点一：验证lcase函数
select lcase('ABc'), lcase('哈哈'), lcase('123456'),lcase('哈市&%%￥#'),lcase(null);

--测试点二：验证lower函数
select lower('ABc'), lower('哈哈'), lower('123456'),lower('哈市&%%￥#'),lower(null);

--测试点三：验证ucase函数
select ucase('ABc'), ucase('哈哈'), ucase('123456'),ucase('哈市&%%￥#'),ucase(null);
select upper('ABc'), upper('哈哈'), upper('123456'),upper('哈市&%%￥#'),upper(null);
--测试点四：验证rand和random_bytes函数
select rand(18446744073709551615) = rand(18446744073709551616);
select rand(-9223372036854775808) = rand(-9223372036854775809);
select rand(0) = rand(4294967296);
select rand(1) = rand(4294967297);
select rand(0) = rand(null);
select random_bytes(null);
select random_bytes(0);
select random_bytes(1);
select random_bytes(1024);
select random_bytes(1025);

set dolphin.b_compatibility_mode to on;
CREATE TABLE test_type_table
(
    `int1` tinyint,
    `uint1` tinyint unsigned,
    `int2` smallint,
    `uint2` smallint unsigned,
    `int4` integer,
    `uint4` integer unsigned,
    `int8` bigint,
    `uint8` bigint unsigned,
    `float4` float4,
    `float8` float8,
    `numeric` decimal(20, 6),
    `bit1` bit(1),
    `bit64` bit(64),
    `boolean` boolean,
    `date` date,
    `time` time,
    `time(4)` time(4),
    `datetime` datetime,
    `datetime(4)` datetime(4) default '2022-11-11 11:11:11',
    `timestamp` timestamp,
    `timestamp(4)` timestamp(4) default '2022-11-11 11:11:11',
    `year` year,
    `char` char(100),
    `varchar` varchar(100),
    `binary` binary(100),
    `varbinary` varbinary(100),
    `tinyblob` tinyblob,
    `blob` blob,
    `mediumblob` mediumblob,
    `longblob` longblob,
    `text` text,
    `enum_t` enum('a', 'b', 'c'),
    `set_t` set('a', 'b', 'c'),
    `json` json
);

insert into test_type_table values(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,b'1', b'111', true,'2023-02-05', '19:10:50', '19:10:50.3456', '2023-02-05 19:10:50', '2023-02-05 19:10:50.456', '2023-02-05 19:10:50', '2023-02-05 19:10:50.456', '2023','1.23a', '1.23a', '1.23a', '1.23a', '1.23a', '1.23a', '1.23a', '1.23a', '1.23a','a', 'a,c',json_object('a', 1, 'b', 2));
select
rand(`int1`)=rand(cast(`int1` as signed)),
rand(`uint1`)=rand(cast(`uint1` as signed)),
rand(`int2`)=rand(cast(`int2` as signed)),
rand(`uint2`)=rand(cast(`uint2` as signed)),
rand(`int4`)=rand(cast(`int4` as signed)),
rand(`uint4`)=rand(cast(`uint4` as signed)),
rand(`int8`)=rand(cast(`int8` as signed)),
rand(`uint8`)=rand(cast(`uint8` as signed)),
rand(`float4`)=rand(cast(`float4` as signed)),
rand(`float8`)=rand(cast(`float8` as signed)),
rand(`numeric`)=rand(cast(`numeric` as signed)),
rand(`bit1`)=rand(cast(`bit1` as signed)),
rand(`bit64`)=rand(cast(`bit64` as signed)),
rand(`boolean`)=rand(cast(`boolean` as signed)),
rand(`date`)=rand(cast(`date` as signed)),
rand(`time`)=rand(cast(`time` as signed)),
rand(`time(4)`)=rand(cast(`time(4)` as signed)),
rand(`datetime`)=rand(cast(`datetime` as signed)),
rand(`datetime(4)`)=rand(cast(`datetime(4)` as signed)),
rand(`timestamp`)=rand(cast(`timestamp` as signed)),
rand(`timestamp(4)`)=rand(cast(`timestamp(4)` as signed)),
rand(`year`)=rand(cast(`year` as signed)),
rand(`char`)=rand(cast(`char` as signed)),
rand(`varchar`)=rand(cast(`varchar` as signed)),
rand(`binary`)=rand(cast(`binary` as signed)),
rand(`varbinary`)=rand(cast(`varbinary` as signed)),
rand(`tinyblob`)=rand(cast(`tinyblob` as signed)),
rand(`blob`)=rand(cast(`blob` as signed)),
rand(`mediumblob`)=rand(cast(`mediumblob` as signed)),
rand(`longblob`)=rand(cast(`longblob` as signed)),
rand(`text`)=rand(cast(`text` as signed)),
rand(`enum_t`)=rand(cast(`enum_t` as signed)),
rand(`set_t`)=rand(cast(`set_t` as signed)),
rand(`json`)=rand(cast(`json` as signed))
from test_type_table;

select
random_bytes(`int1`),
random_bytes(`uint1`),
random_bytes(`int2`),
random_bytes(`uint2`),
random_bytes(`int4`),
random_bytes(`uint4`),
random_bytes(`int8`),
random_bytes(`uint8`),
random_bytes(`float4`),
random_bytes(`float8`),
random_bytes(`numeric`),
random_bytes(`bit1`),
random_bytes(`bit64`),
random_bytes(`boolean`),
random_bytes(`char`),
random_bytes(`varchar`),
random_bytes(`binary`),
random_bytes(`varbinary`),
random_bytes(`tinyblob`),
random_bytes(`blob`),
random_bytes(`mediumblob`),
random_bytes(`longblob`),
random_bytes(`text`),
random_bytes(`enum_t`),
random_bytes(`set_t`)
from test_type_table;

--error, cause value out of range
select length(random_bytes(`date`)::binary) from test_type_table;
select length(random_bytes(`time`)::binary) from test_type_table;
select length(random_bytes(`time(4)`)::binary) from test_type_table;
select length(random_bytes(`datetime`)::binary) from test_type_table;
select length(random_bytes(`datetime(4)`)::binary) from test_type_table;
select length(random_bytes(`timestamp`)::binary) from test_type_table;
select length(random_bytes(`timestamp(4)`)::binary) from test_type_table;
select length(random_bytes(`year`)::binary)from test_type_table;
select length(random_bytes(`json`)::binary)from test_type_table;

--error
Update test_type_table set `year` = rand('{"a":1}'::json);
Update ignore test_type_table set `year` = rand('{"a":1}'::json);
--non-strict mode
set dolphin.sql_mode='';
Update test_type_table set `year` = rand('{"a":1}'::json);
reset dolphin.sql_mode;

drop table test_type_table;

reset dolphin.b_compatibility_mode;

--hex number
select 0x123 = x'123';
select 0x123;
select 0xfe = x'FE';
select 0xFE = x'fe';

select 0xG123; --equal to select 0 xG123;
select 0x123G; --equal to select 0x123 G;
select 0x; --equal to select 0 x;

--测试点五：验证truncate函数
select truncate(111.28);--返回111
select truncate(111.28,0);--返回111
select truncate(111.28,1);--返回111.2
select truncate(111.28,5);
select truncate(111.28,500);
select truncate(111.28,-1);--返回110
select truncate(111.28,-4);--返回0
select  trunc(111.28),  trunc(111.28,0),  trunc(111.28,1),   trunc(111.28,5),   trunc(111.28,500),   trunc(111.28,-1),    trunc(111.28,-4);
--测试点六：验证current_date

--测试点一：验证:=操作符
--step4:建表;expect:创建成功
drop table if exists tb_db_b_parser0003;
create table tb_db_b_parser0003(a1 int, a2 int);
insert into tb_db_b_parser0003 select generate_series(1,5);
--step5:使用:=(赋值)操作符修改a1列值;
update tb_db_b_parser0003 set a1 := 11;
--step6:使用:=(赋值)操作符同时修改a1,a2列值;
update tb_db_b_parser0003 set (a1,a2) := (12,13);
--step7:使用:=(赋值)操作符,修改参数值;
show io_limits; --默认是0
set io_limits := 100;
--alter system set设置参数值
alter system set pagewriter_thread_num := 5;

--测试点二：验证div操作符
select 8div3;--8
select 8 div3;--结果为8
select 8 div 3;
select 8 div 0;

select 8div 3, 8div 3div2, 8div 3div 2;

select 8 div;

select 8 as div;

select div 1;

select '-12.3abc' div null;
select '-12.3abc' div -100.1;
select '-12.3abc' div 0;
select '-12.3abc' div 5;
select '-12.3abc' div 158.3;
select '-12.3abc' div -8.222e4;
select '-12.3abc' div true;
select '-12.3abc' div false;
select '-12.3abc' div 'null';
select 123456 div 5 div 4;
select 8 div 1 where 100 div 3 div 4 = 0;
select 8 div 3 where 100 div 3 div 4 > 0;

set dolphin.b_compatibility_mode to on;
create table tmp_res(a int);
create table div_test(
c1 int1,
c2 int2,
c3 int4,
c4 int8,
c5 uint1,
c6 uint2,
c7 uint4,
c8 uint8,
c9 float,
c10 double,
c11 numeric,
c12 bit,
c13 binary,
c14 interval,
c15 json);
insert into div_test values(0,0,0,0,0,0,0,0,0,0,0,b'0',0,0,'{"0":"0"}');
-- strict mode only, no warning and error
set dolphin.sql_mode = 'sql_mode_strict';
select c1/c1 from div_test;
select c1/c2 from div_test;
select c1/c3 from div_test;
select c1/c4 from div_test;
select c1/c5 from div_test;
select c1/c6 from div_test;
select c1/c7 from div_test;
select c1/c8 from div_test;
select c1/c9 from div_test;
select c1/c10 from div_test;
select c1/c11 from div_test;
select c1/c12 from div_test;
select c1/c13 from div_test;
select c1/c14 from div_test;
select c1/c15 from div_test;
select c1 mod c1 from div_test;
select c1 mod c2 from div_test;
select c1 mod c3 from div_test;
select c1 mod c4 from div_test;
select c1 mod c5 from div_test;
select c1 mod c6 from div_test;
select c1 mod c7 from div_test;
select c1 mod c8 from div_test;
select c1 mod c9 from div_test;
select c1 mod c10 from div_test;
select c1 mod c11 from div_test;
select c1 mod c12 from div_test;
select c1 mod c13 from div_test;
select c1 mod c14 from div_test;
select c1 mod c15 from div_test;
select c1 div c1 from div_test;
select c1 div c2 from div_test;
select c1 div c3 from div_test;
select c1 div c4 from div_test;
select c1 div c5 from div_test;
select c1 div c6 from div_test;
select c1 div c7 from div_test;
select c1 div c8 from div_test;
select c1 div c9 from div_test;
select c1 div c10 from div_test;
select c1 div c11 from div_test;
select c1 div c12 from div_test;
select c1 div c13 from div_test;
select c1 div c14 from div_test;
select c1 div c15 from div_test;

insert into tmp_res select c1/c1 from div_test;
insert into tmp_res select c1/c2 from div_test;
insert into tmp_res select c1/c3 from div_test;
insert into tmp_res select c1/c4 from div_test;
insert into tmp_res select c1/c5 from div_test;
insert into tmp_res select c1/c6 from div_test;
insert into tmp_res select c1/c7 from div_test;
insert into tmp_res select c1/c8 from div_test;
insert into tmp_res select c1/c9 from div_test;
insert into tmp_res select c1/c10 from div_test;
insert into tmp_res select c1/c11 from div_test;
insert into tmp_res select c1/c12 from div_test;
insert into tmp_res select c1/c13 from div_test;
insert into tmp_res select c1/c14 from div_test;
insert into tmp_res select c1/c15 from div_test;
insert into tmp_res select c1 mod c1 from div_test;
insert into tmp_res select c1 mod c2 from div_test;
insert into tmp_res select c1 mod c3 from div_test;
insert into tmp_res select c1 mod c4 from div_test;
insert into tmp_res select c1 mod c5 from div_test;
insert into tmp_res select c1 mod c6 from div_test;
insert into tmp_res select c1 mod c7 from div_test;
insert into tmp_res select c1 mod c8 from div_test;
insert into tmp_res select c1 mod c9 from div_test;
insert into tmp_res select c1 mod c10 from div_test;
insert into tmp_res select c1 mod c11 from div_test;
insert into tmp_res select c1 mod c12 from div_test;
insert into tmp_res select c1 mod c13 from div_test;
insert into tmp_res select c1 mod c14 from div_test;
insert into tmp_res select c1 mod c15 from div_test;
insert into tmp_res select c1 div c1 from div_test;
insert into tmp_res select c1 div c2 from div_test;
insert into tmp_res select c1 div c3 from div_test;
insert into tmp_res select c1 div c4 from div_test;
insert into tmp_res select c1 div c5 from div_test;
insert into tmp_res select c1 div c6 from div_test;
insert into tmp_res select c1 div c7 from div_test;
insert into tmp_res select c1 div c8 from div_test;
insert into tmp_res select c1 div c9 from div_test;
insert into tmp_res select c1 div c10 from div_test;
insert into tmp_res select c1 div c11 from div_test;
insert into tmp_res select c1 div c12 from div_test;
insert into tmp_res select c1 div c13 from div_test;
insert into tmp_res select c1 div c14 from div_test;
insert into tmp_res select c1 div c15 from div_test;

-- non-strict mode and non-err_division_by_zero, no warning and error
set dolphin.sql_mode = '';
select c1/c1 from div_test;
select c1/c2 from div_test;
select c1/c3 from div_test;
select c1/c4 from div_test;
select c1/c5 from div_test;
select c1/c6 from div_test;
select c1/c7 from div_test;
select c1/c8 from div_test;
select c1/c9 from div_test;
select c1/c10 from div_test;
select c1/c11 from div_test;
select c1/c12 from div_test;
select c1/c13 from div_test;
select c1/c14 from div_test;
select c1/c15 from div_test;
select c1 mod c1 from div_test;
select c1 mod c2 from div_test;
select c1 mod c3 from div_test;
select c1 mod c4 from div_test;
select c1 mod c5 from div_test;
select c1 mod c6 from div_test;
select c1 mod c7 from div_test;
select c1 mod c8 from div_test;
select c1 mod c9 from div_test;
select c1 mod c10 from div_test;
select c1 mod c11 from div_test;
select c1 mod c12 from div_test;
select c1 mod c13 from div_test;
select c1 mod c14 from div_test;
select c1 mod c15 from div_test;
select c1 div c1 from div_test;
select c1 div c2 from div_test;
select c1 div c3 from div_test;
select c1 div c4 from div_test;
select c1 div c5 from div_test;
select c1 div c6 from div_test;
select c1 div c7 from div_test;
select c1 div c8 from div_test;
select c1 div c9 from div_test;
select c1 div c10 from div_test;
select c1 div c11 from div_test;
select c1 div c12 from div_test;
select c1 div c13 from div_test;
select c1 div c14 from div_test;
select c1 div c15 from div_test;

insert into tmp_res select c1/c1 from div_test;
insert into tmp_res select c1/c2 from div_test;
insert into tmp_res select c1/c3 from div_test;
insert into tmp_res select c1/c4 from div_test;
insert into tmp_res select c1/c5 from div_test;
insert into tmp_res select c1/c6 from div_test;
insert into tmp_res select c1/c7 from div_test;
insert into tmp_res select c1/c8 from div_test;
insert into tmp_res select c1/c9 from div_test;
insert into tmp_res select c1/c10 from div_test;
insert into tmp_res select c1/c11 from div_test;
insert into tmp_res select c1/c12 from div_test;
insert into tmp_res select c1/c13 from div_test;
insert into tmp_res select c1/c14 from div_test;
insert into tmp_res select c1/c15 from div_test;
insert into tmp_res select c1 mod c1 from div_test;
insert into tmp_res select c1 mod c2 from div_test;
insert into tmp_res select c1 mod c3 from div_test;
insert into tmp_res select c1 mod c4 from div_test;
insert into tmp_res select c1 mod c5 from div_test;
insert into tmp_res select c1 mod c6 from div_test;
insert into tmp_res select c1 mod c7 from div_test;
insert into tmp_res select c1 mod c8 from div_test;
insert into tmp_res select c1 mod c9 from div_test;
insert into tmp_res select c1 mod c10 from div_test;
insert into tmp_res select c1 mod c11 from div_test;
insert into tmp_res select c1 mod c12 from div_test;
insert into tmp_res select c1 mod c13 from div_test;
insert into tmp_res select c1 mod c14 from div_test;
insert into tmp_res select c1 mod c15 from div_test;
insert into tmp_res select c1 div c1 from div_test;
insert into tmp_res select c1 div c2 from div_test;
insert into tmp_res select c1 div c3 from div_test;
insert into tmp_res select c1 div c4 from div_test;
insert into tmp_res select c1 div c5 from div_test;
insert into tmp_res select c1 div c6 from div_test;
insert into tmp_res select c1 div c7 from div_test;
insert into tmp_res select c1 div c8 from div_test;
insert into tmp_res select c1 div c9 from div_test;
insert into tmp_res select c1 div c10 from div_test;
insert into tmp_res select c1 div c11 from div_test;
insert into tmp_res select c1 div c12 from div_test;
insert into tmp_res select c1 div c13 from div_test;
insert into tmp_res select c1 div c14 from div_test;
insert into tmp_res select c1 div c15 from div_test;

-- strict mode and err_division_by_zero, select warning, insert error
set dolphin.sql_mode = 'sql_mode_strict,ERROR_FOR_DIVISION_BY_ZERO';
select c1/c1 from div_test;
select c1/c2 from div_test;
select c1/c3 from div_test;
select c1/c4 from div_test;
select c1/c5 from div_test;
select c1/c6 from div_test;
select c1/c7 from div_test;
select c1/c8 from div_test;
select c1/c9 from div_test;
select c1/c10 from div_test;
select c1/c11 from div_test;
select c1/c12 from div_test;
select c1/c13 from div_test;
select c1/c14 from div_test;
select c1/c15 from div_test;
select c1 mod c1 from div_test;
select c1 mod c2 from div_test;
select c1 mod c3 from div_test;
select c1 mod c4 from div_test;
select c1 mod c5 from div_test;
select c1 mod c6 from div_test;
select c1 mod c7 from div_test;
select c1 mod c8 from div_test;
select c1 mod c9 from div_test;
select c1 mod c10 from div_test;
select c1 mod c11 from div_test;
select c1 mod c12 from div_test;
select c1 mod c13 from div_test;
select c1 mod c14 from div_test;
select c1 mod c15 from div_test;
select c1 div c1 from div_test;
select c1 div c2 from div_test;
select c1 div c3 from div_test;
select c1 div c4 from div_test;
select c1 div c5 from div_test;
select c1 div c6 from div_test;
select c1 div c7 from div_test;
select c1 div c8 from div_test;
select c1 div c9 from div_test;
select c1 div c10 from div_test;
select c1 div c11 from div_test;
select c1 div c12 from div_test;
select c1 div c13 from div_test;
select c1 div c14 from div_test;
select c1 div c15 from div_test;

insert into tmp_res select c1/c1 from div_test;
insert into tmp_res select c1/c2 from div_test;
insert into tmp_res select c1/c3 from div_test;
insert into tmp_res select c1/c4 from div_test;
insert into tmp_res select c1/c5 from div_test;
insert into tmp_res select c1/c6 from div_test;
insert into tmp_res select c1/c7 from div_test;
insert into tmp_res select c1/c8 from div_test;
insert into tmp_res select c1/c9 from div_test;
insert into tmp_res select c1/c10 from div_test;
insert into tmp_res select c1/c11 from div_test;
insert into tmp_res select c1/c12 from div_test;
insert into tmp_res select c1/c13 from div_test;
insert into tmp_res select c1/c14 from div_test;
insert into tmp_res select c1/c15 from div_test;
insert into tmp_res select c1 mod c1 from div_test;
insert into tmp_res select c1 mod c2 from div_test;
insert into tmp_res select c1 mod c3 from div_test;
insert into tmp_res select c1 mod c4 from div_test;
insert into tmp_res select c1 mod c5 from div_test;
insert into tmp_res select c1 mod c6 from div_test;
insert into tmp_res select c1 mod c7 from div_test;
insert into tmp_res select c1 mod c8 from div_test;
insert into tmp_res select c1 mod c9 from div_test;
insert into tmp_res select c1 mod c10 from div_test;
insert into tmp_res select c1 mod c11 from div_test;
insert into tmp_res select c1 mod c12 from div_test;
insert into tmp_res select c1 mod c13 from div_test;
insert into tmp_res select c1 mod c14 from div_test;
insert into tmp_res select c1 mod c15 from div_test;
insert into tmp_res select c1 div c1 from div_test;
insert into tmp_res select c1 div c2 from div_test;
insert into tmp_res select c1 div c3 from div_test;
insert into tmp_res select c1 div c4 from div_test;
insert into tmp_res select c1 div c5 from div_test;
insert into tmp_res select c1 div c6 from div_test;
insert into tmp_res select c1 div c7 from div_test;
insert into tmp_res select c1 div c8 from div_test;
insert into tmp_res select c1 div c9 from div_test;
insert into tmp_res select c1 div c10 from div_test;
insert into tmp_res select c1 div c11 from div_test;
insert into tmp_res select c1 div c12 from div_test;
insert into tmp_res select c1 div c13 from div_test;
insert into tmp_res select c1 div c14 from div_test;
insert into tmp_res select c1 div c15 from div_test;

-- non-strict mode and err_division_by_zero, select warning, insert warning
set dolphin.sql_mode = 'ERROR_FOR_DIVISION_BY_ZERO';
select c1/c1 from div_test;
select c1/c2 from div_test;
select c1/c3 from div_test;
select c1/c4 from div_test;
select c1/c5 from div_test;
select c1/c6 from div_test;
select c1/c7 from div_test;
select c1/c8 from div_test;
select c1/c9 from div_test;
select c1/c10 from div_test;
select c1/c11 from div_test;
select c1/c12 from div_test;
select c1/c13 from div_test;
select c1/c14 from div_test;
select c1/c15 from div_test;
select c1 mod c1 from div_test;
select c1 mod c2 from div_test;
select c1 mod c3 from div_test;
select c1 mod c4 from div_test;
select c1 mod c5 from div_test;
select c1 mod c6 from div_test;
select c1 mod c7 from div_test;
select c1 mod c8 from div_test;
select c1 mod c9 from div_test;
select c1 mod c10 from div_test;
select c1 mod c11 from div_test;
select c1 mod c12 from div_test;
select c1 mod c13 from div_test;
select c1 mod c14 from div_test;
select c1 mod c15 from div_test;
select c1 div c1 from div_test;
select c1 div c2 from div_test;
select c1 div c3 from div_test;
select c1 div c4 from div_test;
select c1 div c5 from div_test;
select c1 div c6 from div_test;
select c1 div c7 from div_test;
select c1 div c8 from div_test;
select c1 div c9 from div_test;
select c1 div c10 from div_test;
select c1 div c11 from div_test;
select c1 div c12 from div_test;
select c1 div c13 from div_test;
select c1 div c14 from div_test;
select c1 div c15 from div_test;

insert into tmp_res select c1/c1 from div_test;
insert into tmp_res select c1/c2 from div_test;
insert into tmp_res select c1/c3 from div_test;
insert into tmp_res select c1/c4 from div_test;
insert into tmp_res select c1/c5 from div_test;
insert into tmp_res select c1/c6 from div_test;
insert into tmp_res select c1/c7 from div_test;
insert into tmp_res select c1/c8 from div_test;
insert into tmp_res select c1/c9 from div_test;
insert into tmp_res select c1/c10 from div_test;
insert into tmp_res select c1/c11 from div_test;
insert into tmp_res select c1/c12 from div_test;
insert into tmp_res select c1/c13 from div_test;
insert into tmp_res select c1/c14 from div_test;
insert into tmp_res select c1/c15 from div_test;
insert into tmp_res select c1 mod c1 from div_test;
insert into tmp_res select c1 mod c2 from div_test;
insert into tmp_res select c1 mod c3 from div_test;
insert into tmp_res select c1 mod c4 from div_test;
insert into tmp_res select c1 mod c5 from div_test;
insert into tmp_res select c1 mod c6 from div_test;
insert into tmp_res select c1 mod c7 from div_test;
insert into tmp_res select c1 mod c8 from div_test;
insert into tmp_res select c1 mod c9 from div_test;
insert into tmp_res select c1 mod c10 from div_test;
insert into tmp_res select c1 mod c11 from div_test;
insert into tmp_res select c1 mod c12 from div_test;
insert into tmp_res select c1 mod c13 from div_test;
insert into tmp_res select c1 mod c14 from div_test;
insert into tmp_res select c1 mod c15 from div_test;
insert into tmp_res select c1 div c1 from div_test;
insert into tmp_res select c1 div c2 from div_test;
insert into tmp_res select c1 div c3 from div_test;
insert into tmp_res select c1 div c4 from div_test;
insert into tmp_res select c1 div c5 from div_test;
insert into tmp_res select c1 div c6 from div_test;
insert into tmp_res select c1 div c7 from div_test;
insert into tmp_res select c1 div c8 from div_test;
insert into tmp_res select c1 div c9 from div_test;
insert into tmp_res select c1 div c10 from div_test;
insert into tmp_res select c1 div c11 from div_test;
insert into tmp_res select c1 div c12 from div_test;
insert into tmp_res select c1 div c13 from div_test;
insert into tmp_res select c1 div c14 from div_test;
insert into tmp_res select c1 div c15 from div_test;
select count(*) from tmp_res;
select count(*) from tmp_res where a is null;
drop table tmp_res;
drop table div_test;

--测试点三：验证mod操作符
select 8mod3;--返回mod3 8
select 8 mod3;--返回mod3 8
select 8 mod 3; 
select 8 mod 0;
select 8mod 3, 8mod 3mod2, 8mod 3mod 2;
select 8 mod;--返回8
select 8 as mod;--返回mod 8
select mod 1;

select '-12.3abc' mod null;
select '-12.3abc' mod -100.1;
select '-12.3abc' mod 0;
select '-12.3abc' mod 5;
select '-12.3abc' mod 158.3;
select '-12.3abc' mod -8.222e4;
select '-12.3abc' mod true;
select '-12.3abc' mod false;
select '-12.3abc' mod 'null';
select 123456 mod 5 mod 4;
select 8 mod 1 where 100 mod 3 mod 4 = 0;
select 8 mod 3 where 100 mod 3 mod 4 > 0;

--测试点四：验证xor操作符

select xor1;

select 1 xor 1;
select 1 xor null;
select null xor 1;
select 1 xor 0;
select '-12.3abc' xor null;
select '-12.3abc' xor -100.1;
select '-12.3abc' xor 0;
select '-12.3abc' xor 5;
select '-12.3abc' xor 158.3;
select '-12.3abc' xor -8.222e4;
select '-12.3abc' xor true;
select '-12.3abc' xor false;
select '-12.3abc' xor 'null';

create table t1 (a int, b varchar(20));
insert into t1 values(1, 'true');

select a = 1 xor b = 'true' from t1;

select acos(11);
select acos(1.000001);
select acos(-1.000001);

drop schema db_b_parser3 cascade;
reset current_schema;
