set dolphin.b_compatibility_mode to off;
drop database if exists db_b_atan_test;
NOTICE:  database "db_b_atan_test" does not exist, skipping
create database db_b_atan_test dbcompatibility 'A';
\c db_b_atan_test
select atan(-2, 2);
ERROR:  function atan(integer, integer) does not exist
LINE 1: select atan(-2, 2);
               ^
HINT:  No function matches the given name and argument types. You might need to add explicit type casts.
CONTEXT:  referenced column: atan
-- error
select atan2(-2, 2);
       atan2       
-------------------
 -.785398163397448
(1 row)

-- -0.7853981633974483
select atan2(pi(), 0);
      atan2      
-----------------
 1.5707963267949
(1 row)

-- 1.5707963267948966
select pi(),atan(1);
        pi        |       atan       
------------------+------------------
 3.14159265358979 | .785398163397448
(1 row)

-- 3.141593 | 0.7853981633974483
SELECT ATAN2(1, 1); 
      atan2       
------------------
 .785398163397448
(1 row)

-- 0.7853981633974483
SELECT ATAN2(0.5, 0.5); 
      atan2       
------------------
 .785398163397448
(1 row)

-- 0.7853981633974483
SELECT ATAN2(-1, 1); 
       atan2       
-------------------
 -.785398163397448
(1 row)

-- -0.7853981633974483
SELECT ATAN2(-0.5, 0.5);
       atan2       
-------------------
 -.785398163397448
(1 row)

-- -0.7853981633974483
\c contrib_regression
set dolphin.b_compatibility_mode to off;
drop database db_b_atan_test;
set dolphin.b_compatibility_mode to on;
create schema db_b_atan_test;
set current_schema to 'db_b_atan_test';
-- 基本功能
select atan(-2, 2);
        atan         
---------------------
 -0.7853981633974483
(1 row)

-- -0.7853981633974483
select atan(pi(), 0);
       atan        
-------------------
 1.570796326794897
(1 row)

-- 1.5707963267948966
select pi(),atan(1);
        pi         |        atan        
-------------------+--------------------
 3.141592653589793 | 0.7853981633974483
(1 row)

-- 3.141593 | 0.7853981633974483
SELECT ATAN(1, 1); 
        atan        
--------------------
 0.7853981633974483
(1 row)

-- 0.7853981633974483
SELECT ATAN(0.5, 0.5); 
        atan        
--------------------
 0.7853981633974483
(1 row)

-- 0.7853981633974483
SELECT ATAN(-1, 1); 
        atan         
---------------------
 -0.7853981633974483
(1 row)

-- -0.7853981633974483
SELECT ATAN(-0.5, 0.5);
        atan         
---------------------
 -0.7853981633974483
(1 row)

-- -0.7853981633974483
SELECT ATAN('42'::integer, '21.0'::double precision);
       atan       
------------------
 1.10714871779409
(1 row)

-- 1.10714871779409
SELECT ATAN('42'::bigint, '21'::smallint);
       atan       
------------------
 1.10714871779409
(1 row)

-- 1.10714871779409
SELECT ATAN('42':: real, '21.0':: numeric(10,2));
       atan       
------------------
 1.10714871779409
(1 row)

-- 1.10714871779409
-- boolean类型
SELECT ATAN(true, false);
       atan        
-------------------
 1.570796326794897
(1 row)

-- 1.5707963267948966
SELECT ATAN(true, 0.5);
       atan       
------------------
 1.10714871779409
(1 row)

-- 1.1071487177940904
SELECT ATAN(0.5, true);
        atan        
--------------------
 0.4636476090008061
(1 row)

-- 0.4636476090008061
SELECT ATAN(2, true);
       atan       
------------------
 1.10714871779409
(1 row)

-- 1.1071487177940904
SELECT ATAN(true);
        atan        
--------------------
 0.7853981633974483
(1 row)

-- 0.7853981633974483
-- 空值测试
SELECT ATAN(NULL, 1); 
 atan 
------
     
(1 row)

-- NULL
SELECT ATAN(1, NULL); 
 atan 
------
     
(1 row)

-- NULL
SELECT ATAN(NULL, NULL); 
 atan 
------
     
(1 row)

-- NULL
-- 超出取值范围的入参测试	
SELECT ATAN(1E+308, 1E+308); 
        atan        
--------------------
 0.7853981633974483
(1 row)

-- 0.7853981633974483
SELECT ATAN(-1E+308, 1E+308); 
        atan         
---------------------
 -0.7853981633974483
(1 row)

-- -0.7853981633974483
SELECT ATAN(1E+309, 1E+308); 
WARNING:  "1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" is out of range for type double precision
CONTEXT:  referenced column: atan
       atan        
-------------------
 1.063153217141895
(1 row)

-- Error
SELECT ATAN(-1E+309, 1E+308); 
WARNING:  "-1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" is out of range for type double precision
CONTEXT:  referenced column: atan
        atan        
--------------------
 -1.063153217141895
(1 row)

-- Error
-- 非数值型入参测试
SELECT ATAN('1', 1); 
        atan        
--------------------
 0.7853981633974483
(1 row)

-- 0.7853981633974483
SELECT ATAN(1, '1'); 
        atan        
--------------------
 0.7853981633974483
(1 row)

-- 0.7853981633974483
SELECT ATAN(2, '1'::character(1));
       atan       
------------------
 1.10714871779409
(1 row)

-- 1.10714871779409
SELECT ATAN('2'::text, 1);
       atan       
------------------
 1.10714871779409
(1 row)

-- 1.10714871779409
SELECT ATAN('a', 1); 
WARNING:  invalid input syntax for type double precision: "a"
LINE 1: SELECT ATAN('a', 1);
                    ^
CONTEXT:  referenced column: atan
 atan 
------
    0
(1 row)

-- ERROR
SELECT ATAN(2, '1'::boolean);
       atan       
------------------
 1.10714871779409
(1 row)

-- 1.1071487177940904
SELECT ATAN('2'::interval, 1);
ERROR:  function atan(interval, integer) does not exist
LINE 1: SELECT ATAN('2'::interval, 1);
               ^
HINT:  No function matches the given name and argument types. You might need to add explicit type casts.
CONTEXT:  referenced column: atan
-- ERROR
SELECT ATAN('2022-05-12', 1);
WARNING:  invalid input syntax for type double precision: "2022-05-12"
LINE 1: SELECT ATAN('2022-05-12', 1);
                    ^
CONTEXT:  referenced column: atan
       atan        
-------------------
 1.570301766993477
(1 row)

-- ERROR
SELECT ATAN('2022-05-12'::date, 1);
       atan        
-------------------
 1.570796277340165
(1 row)

-- 1.57079627734016
SELECT ATAN('2023-06-01 00:23:59'::timestamp, 1);
       atan        
-------------------
 1.570796326794847
(1 row)

-- 1.57079632679485
SELECT ATAN('2'::bytea, 1);
       atan       
------------------
 1.10714871779409
(1 row)

-- 1.10714871779409
SELECT ATAN(ARRAY[1,2,3], 1);
ERROR:  function atan(integer[], integer) does not exist
LINE 1: SELECT ATAN(ARRAY[1,2,3], 1);
               ^
HINT:  No function matches the given name and argument types. You might need to add explicit type casts.
CONTEXT:  referenced column: atan
-- ERROR
drop schema db_b_atan_test cascade;
reset current_schema;
