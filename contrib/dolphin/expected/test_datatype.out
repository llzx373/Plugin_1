create schema b_datatype_test;
set current_schema to 'b_datatype_test';
-- bit(n), when insert into bit, support the length less than n, which must be equal to n in normal case
create table bit_test(a bit);
create table bit_test2(a bit(5));
create table bit_test3(a bit(35));
insert into bit_test values(b'');
update bit_test set a = b'1' where a = b'';
insert into bit_test values(b'0');
update bit_test set a = b'' where a = b'0';
select * from bit_test order by 1;
 a 
---
 0
 1
(2 rows)

--error, too long
update bit_test set a = b'00' where a = b'0';
delete from bit_test where a=b'0';
delete from bit_test where a=b'1';
delete from bit_test where a=b'';
delete from bit_test where a=b'1111';
select * from bit_test order by 1;
 a 
---
(0 rows)

--error, too long
insert into bit_test values(b'11');
ERROR:  bit string length 2 does not match type bit(1)
CONTEXT:  referenced column: a
insert into bit_test2 values(b'');
update bit_test2 set a = b'0' where a = b'';
insert into bit_test2 values(b'1');
update bit_test2 set a = b'0' where a = b'1';
insert into bit_test2 values(b'11');
update bit_test2 set a = b'00' where a = b'11';
insert into bit_test2 values(b'111');
update bit_test2 set a = b'000' where a = b'111';
insert into bit_test2 values(b'1111');
update bit_test2 set a = b'0000' where a = b'1111';
insert into bit_test2 values(b'11111');
update bit_test2 set a = b'00000' where a = b'11111';
select * from bit_test2 order by 1;
   a   
-------
 00000
 00000
 00000
 00000
 00000
 00000
(6 rows)

--error, too long
insert into bit_test2 values(b'111111');
ERROR:  bit string length 6 does not match type bit(5)
CONTEXT:  referenced column: a
update bit_test2 set a = b'111111' where a = b'00000';
ERROR:  bit string length 6 does not match type bit(5)
CONTEXT:  referenced column: a
delete from bit_test2 where a=b'0000000000';
select * from bit_test2 order by 1;
 a 
---
(0 rows)

insert into bit_test3 values(b'');
insert into bit_test3 values(b'1');
select * from bit_test3 where a=b'' order by 1;
                  a                  
-------------------------------------
 00000000000000000000000000000000000
(1 row)

select * from bit_test3 where a=b'0' order by 1;
                  a                  
-------------------------------------
 00000000000000000000000000000000000
(1 row)

select * from bit_test3 where a=b'00' order by 1;
                  a                  
-------------------------------------
 00000000000000000000000000000000000
(1 row)

select * from bit_test3 where a=b'000' order by 1;
                  a                  
-------------------------------------
 00000000000000000000000000000000000
(1 row)

select * from bit_test3 where a=b'01' order by 1;
                  a                  
-------------------------------------
 00000000000000000000000000000000001
(1 row)

select * from bit_test3 where a=b'000000000000000000000000000000000000000000000000000000000000000000000001' order by 1;
                  a                  
-------------------------------------
 00000000000000000000000000000000001
(1 row)

select * from bit_test3 where a=b'10' order by 1;
 a 
---
(0 rows)

select * from bit_test3 where a>b'';
                  a                  
-------------------------------------
 00000000000000000000000000000000001
(1 row)

select * from bit_test3 where a<b'';
 a 
---
(0 rows)

select * from bit_test3 where a>=b'';
                  a                  
-------------------------------------
 00000000000000000000000000000000000
 00000000000000000000000000000000001
(2 rows)

select * from bit_test3 where a<=b'';
                  a                  
-------------------------------------
 00000000000000000000000000000000000
(1 row)

select * from bit_test3 where a>b'1';
 a 
---
(0 rows)

select * from bit_test3 where a<b'1';
                  a                  
-------------------------------------
 00000000000000000000000000000000000
(1 row)

select * from bit_test3 where a>=b'1';
                  a                  
-------------------------------------
 00000000000000000000000000000000001
(1 row)

select * from bit_test3 where a<=b'1';
                  a                  
-------------------------------------
 00000000000000000000000000000000000
 00000000000000000000000000000000001
(2 rows)

insert into bit_test2 values(b'');
insert into bit_test2 values(b'001');
select * from bit_test2 order by 1;
   a   
-------
 00000
 00001
(2 rows)

explain(costs off) select * from bit_test2 a left join bit_test3 b on a.a=b.a;
             QUERY PLAN              
-------------------------------------
 Merge Right Join
   Merge Cond: (b.a = a.a)
   ->  Sort
         Sort Key: b.a
         ->  Seq Scan on bit_test3 b
   ->  Sort
         Sort Key: a.a
         ->  Seq Scan on bit_test2 a
(8 rows)

select * from bit_test2 a left join bit_test3 b on a.a=b.a order by 1;
   a   |                  a                  
-------+-------------------------------------
 00000 | 00000000000000000000000000000000000
 00001 | 00000000000000000000000000000000001
(2 rows)

set enable_mergejoin to off;
explain(costs off) select * from bit_test2 a left join bit_test3 b on a.a=b.a;
             QUERY PLAN              
-------------------------------------
 Nested Loop Left Join
   Join Filter: (a.a = b.a)
   ->  Seq Scan on bit_test2 a
   ->  Materialize
         ->  Seq Scan on bit_test3 b
(5 rows)

select * from bit_test2 a left join bit_test3 b on a.a=b.a order by 1;
   a   |                  a                  
-------+-------------------------------------
 00000 | 00000000000000000000000000000000000
 00001 | 00000000000000000000000000000000001
(2 rows)

drop table bit_test;
drop table bit_test2;
drop table bit_test3;
select b'11'::bit(4);
 bit  
------
 0011
(1 row)

select b'11'::bit(8);
   bit    
----------
 00000011
(1 row)

select b'11'::bit(9);
    bit    
-----------
 000000011
(1 row)

select b'11'::bit(15);
       bit       
-----------------
 000000000000011
(1 row)

select b'11'::bit(16);
       bit        
------------------
 0000000000000011
(1 row)

select b'11'::bit(17);
        bit        
-------------------
 00000000000000011
(1 row)

select b'11'::bit(32);
               bit                
----------------------------------
 00000000000000000000000000000011
(1 row)

select b'11'::bit(33);
                bit                
-----------------------------------
 000000000000000000000000000000011
(1 row)

--tinyint(n),smallint(n),mediumint,mediumint(n),int(n),bigint(n)
create table all_int_test(a tinyint(9999999999), b smallint(9999999999), c mediumint, d mediumint(9999999999), e int(9999999999), f bigint(9999999999));
\d all_int_test
Table "b_datatype_test.all_int_test"
 Column |   Type   | Modifiers 
--------+----------+-----------
 a      | tinyint  | 
 b      | smallint | 
 c      | integer  | 
 d      | integer  | 
 e      | integer  | 
 f      | bigint   | 

drop table all_int_test;
CREATE TABLE t0(c0 VARCHAR, c1 VARCHAR);
INSERT INTO t0 VALUES (0,0);
UPDATE t0 SET c0=0;
select * from t0 order by 1,2;
 c0 | c1 
----+----
 0  | 0
(1 row)

UPDATE t0 SET c0=true,c1='true';
select * from t0 order by 1,2;
 c0 |  c1  
----+------
 1  | true
(1 row)

INSERT INTO t0 VALUES (1,'1'),(true,'true'),(false,'false');
SELECT * FROM t0 WHERE t0.c0 = true order by 1,2;
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: 0
 c0 |  c1  
----+------
 1  | 1
 1  | true
 1  | true
(3 rows)

SELECT * FROM t0 WHERE t0.c0 is true order by 1,2;
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: 0
 c0 |  c1  
----+------
 1  | 1
 1  | true
 1  | true
(3 rows)

SELECT * FROM t0 WHERE t0.c0 is false order by 1,2;
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: 0
 c0 |  c1   
----+-------
 0  | false
(1 row)

SELECT * FROM t0 WHERE t0.c1 = true order by 1,2;
WARNING:  Truncated incorrect DOUBLE value: true
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: true
WARNING:  Truncated incorrect DOUBLE value: false
 c0 | c1 
----+----
 1  | 1
(1 row)

SELECT * FROM t0 WHERE t0.c1 is true order by 1,2;
WARNING:  Truncated incorrect DOUBLE value: true
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: true
WARNING:  Truncated incorrect DOUBLE value: false
 c0 | c1 
----+----
 1  | 1
(1 row)

SELECT * FROM t0 WHERE t0.c1 is false order by 1,2;
WARNING:  Truncated incorrect DOUBLE value: true
WARNING:  Truncated incorrect DOUBLE value: 1
WARNING:  Truncated incorrect DOUBLE value: true
WARNING:  Truncated incorrect DOUBLE value: false
 c0 |  c1   
----+-------
 0  | false
 1  | true
 1  | true
(3 rows)

drop table t0;
--test for set
drop table if exists set_tab;
NOTICE:  table "set_tab" does not exist, skipping
create table set_tab (
  c1 set('1','2','3','4','5'),
  c2 set('a', 'b', 'c', 'd', 'e')
);
NOTICE:  CREATE TABLE will create implicit set "set_tab_c1_set" for column "set_tab.c1"
NOTICE:  CREATE TABLE will create implicit set "set_tab_c2_set" for column "set_tab.c2"
insert into set_tab values('1,2','a,b');
insert into set_tab values('3,4','c,d');
create or replace function gettypeid(tname text) returns INT4 as
$$
begin
    return oid from pg_type where typname = :tname;
end;
$$ language plpgsql;
select varlenatoset(c1,gettypeid('set_tab_c1_set')) from set_tab order by 1;
 varlenatoset 
--------------
 1,2
 3,4
(2 rows)

select varlenatoset(c2,gettypeid('set_tab_c2_set')) from set_tab order by 1;
 varlenatoset 
--------------
 a,b
 c,d
(2 rows)

drop function gettypeid;
drop table set_tab;
----
set dolphin.b_compatibility_mode = true;
drop table if exists t1;
NOTICE:  table "t1" does not exist, skipping
create table t1 (a char(16), b date, c datetime);
insert into t1 SET a='test 2000-01-01', b='2000-01-01', c='2000-01-01';
select * from t1 where c = '2000-01-01';
        a         |     b      |          c          
------------------+------------+---------------------
 test 2000-01-01  | 2000-01-01 | 2000-01-01 00:00:00
(1 row)

select * from t1 where b = '2000-01-01';
        a         |     b      |          c          
------------------+------------+---------------------
 test 2000-01-01  | 2000-01-01 | 2000-01-01 00:00:00
(1 row)

select * from t1 where c = '2000-01-01'::datetime;
        a         |     b      |          c          
------------------+------------+---------------------
 test 2000-01-01  | 2000-01-01 | 2000-01-01 00:00:00
(1 row)

select * from t1 where b = '2000-01-01'::date;
        a         |     b      |          c          
------------------+------------+---------------------
 test 2000-01-01  | 2000-01-01 | 2000-01-01 00:00:00
(1 row)

set dolphin.b_compatibility_mode = off;
select * from t1 where c = '2000-01-01';
        a         |     b      |          c          
------------------+------------+---------------------
 test 2000-01-01  | 2000-01-01 | 2000-01-01 00:00:00
(1 row)

select * from t1 where b = '2000-01-01';
        a         |     b      |          c          
------------------+------------+---------------------
 test 2000-01-01  | 2000-01-01 | 2000-01-01 00:00:00
(1 row)

select * from t1 where c = '2000-01-01'::datetime;
        a         |     b      |          c          
------------------+------------+---------------------
 test 2000-01-01  | 2000-01-01 | 2000-01-01 00:00:00
(1 row)

select * from t1 where b = '2000-01-01'::date;
        a         |     b      |          c          
------------------+------------+---------------------
 test 2000-01-01  | 2000-01-01 | 2000-01-01 00:00:00
(1 row)

drop table t1;
---
select pg_catalog.delete(cast('test=>NULL' as hstore), cast(pg_catalog.dolphin_types() as _text));
    delete    
--------------
 "test"=>NULL
(1 row)

drop schema b_datatype_test cascade;
reset current_schema;
