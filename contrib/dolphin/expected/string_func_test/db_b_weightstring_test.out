create schema db_b_weightstring_test;
set current_schema to 'db_b_weightstring_test';
-- 基准测试
select hex(weight_string('aa' as char(3)));
     hex      
--------------
 004100410020
(1 row)

select hex(weight_string('a' as char(1)));
 hex  
------
 0041
(1 row)

select hex(weight_string('ab' as char(1)));
 hex  
------
 0041
(1 row)

select hex(weight_string('ab'));
   hex    
----------
 00410042
(1 row)

select hex(weight_string('aa' as binary(3)));
  hex   
--------
 616100
(1 row)

select hex(weight_string('abc' as binary(2)));
 hex  
------
 6162
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 1 ));
   hex    
----------
 00410042
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 1 ASC));
   hex    
----------
 00410042
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 1 DESC));
   hex    
----------
 FFBEFFBD
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 1 REVERSE));
   hex    
----------
 42004100
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 1 DESC REVERSE));
   hex    
----------
 BDFFBEFF
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 1 DESC, 1 REVERSE));
   hex    
----------
 BDFFBEFF
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 1 DESC, 2 REVERSE));
   hex    
----------
 FFBEFFBD
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 2 DESC, 1 REVERSE));
   hex    
----------
 42004100
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 2 DESC  REVERSE));
   hex    
----------
 00410042
(1 row)

select hex(weight_string('abc' as char(2) LEVEL 2,3,5 REVERSE));
   hex    
----------
 00410042
(1 row)

select hex(weight_string(cast('aa' as binary(3))));
  hex   
--------
 616100
(1 row)

select hex(weight_string(123 as char(4)));
 hex 
-----
 
(1 row)

select hex(weight_string(123.25 as char(4)));
 hex 
-----
 
(1 row)

-- 其他literal
SELECT HEX(WEIGHT_STRING(E'\x74\x65\x73\x74' as char(10)));
                   hex                    
------------------------------------------
 0054004500530054002000200020002000200020
(1 row)

SELECT HEX(WEIGHT_STRING(E'\\x74657374' as char(10)));
                   hex                    
------------------------------------------
 005C005800370034003600350037003300370034
(1 row)

SELECT HEX(WEIGHT_STRING(U&'\0074\0065\0073\0074' as char(10)));
                   hex                    
------------------------------------------
 0054004500530054002000200020002000200020
(1 row)

SELECT HEX(WEIGHT_STRING('123456789'::integer as char(10)));
 hex 
-----
 
(1 row)

SELECT HEX(WEIGHT_STRING('123456789.0'::real as char(20)));
 hex 
-----
 
(1 row)

SELECT HEX(WEIGHT_STRING('123456789.0'::double precision as char(20)));
 hex 
-----
 
(1 row)

SELECT HEX(WEIGHT_STRING('1'::boolean as char(10)));
 hex 
-----
 
(1 row)

SELECT HEX(WEIGHT_STRING('123456789'::character(9) as char(20)));
                                       hex                                        
----------------------------------------------------------------------------------
 00310032003300340035003600370038003900200020002000200020002000200020002000200020
(1 row)

SELECT HEX(WEIGHT_STRING('123456789'::text as char(20)));
                                       hex                                        
----------------------------------------------------------------------------------
 00310032003300340035003600370038003900200020002000200020002000200020002000200020
(1 row)

SELECT HEX(WEIGHT_STRING('123456789'::bytea as char(20)));
                                       hex                                        
----------------------------------------------------------------------------------
 00310032003300340035003600370038003900200020002000200020002000200020002000200020
(1 row)

SELECT HEX(WEIGHT_STRING('2023-06-01 00:23:59'::date as char(20)));
                   hex                    
------------------------------------------
 323032332D30362D303120202020202020202020
(1 row)

SELECT HEX(WEIGHT_STRING('2023-06-01 00:23:59'::datetime as char(20)));
                   hex                    
------------------------------------------
 323032332D30362D30312030303A32333A353920
(1 row)

SELECT HEX(WEIGHT_STRING('2023-06-01 00:23:59'::time as char(20)));
                   hex                    
------------------------------------------
 30303A32333A3539202020202020202020202020
(1 row)

SELECT HEX(WEIGHT_STRING('2023-06-01 00:23:59'::timestamp as char(20)));
                   hex                    
------------------------------------------
 323032332D30362D30312030303A32333A35392D
(1 row)

SELECT HEX(WEIGHT_STRING('2021-11-4 16:30:44.3411'::timestamptz as char(20)));
                   hex                    
------------------------------------------
 323032312D31312D30342031363A33303A34342E
(1 row)

SELECT HEX(WEIGHT_STRING('59 minute'::interval as char(20)));
                   hex                    
------------------------------------------
 40203539206D696E732020202020202020202020
(1 row)

SELECT HEX(WEIGHT_STRING(date '2021-12-31' as char(10)));
         hex          
----------------------
 323032312D31322D3331
(1 row)

SELECT HEX(WEIGHT_STRING(E'\x74\x65\x73\x74' as char(10)));
                   hex                    
------------------------------------------
 0054004500530054002000200020002000200020
(1 row)

SELECT HEX(WEIGHT_STRING(E'\\x74657374' as char(10)));
                   hex                    
------------------------------------------
 005C005800370034003600350037003300370034
(1 row)

SELECT HEX(WEIGHT_STRING(U&'\0074\0065\0073\0074' as char(10)));
                   hex                    
------------------------------------------
 0054004500530054002000200020002000200020
(1 row)

SELECT HEX(WEIGHT_STRING('123456789'::integer as binary(10)));
 hex 
-----
 
(1 row)

SELECT HEX(WEIGHT_STRING('123456789.0'::real as binary (20)));
 hex 
-----
 
(1 row)

SELECT HEX(WEIGHT_STRING('123456789.0'::double precision as binary (20)));
 hex 
-----
 
(1 row)

SELECT HEX(WEIGHT_STRING('1'::boolean as binary(10)));
         hex          
----------------------
 31000000000000000000
(1 row)

SELECT HEX(WEIGHT_STRING('123456789'::character(9) as binary (20)));
                   hex                    
------------------------------------------
 3132333435363738390000000000000000000000
(1 row)

SELECT HEX(WEIGHT_STRING('123456789'::text as binary (20)));
                   hex                    
------------------------------------------
 3132333435363738390000000000000000000000
(1 row)

SELECT HEX(WEIGHT_STRING('123456789'::bytea as binary (20)));
                   hex                    
------------------------------------------
 3132333435363738390000000000000000000000
(1 row)

SELECT HEX(WEIGHT_STRING('2023-06-01 00:23:59'::date as binary (20)));
                   hex                    
------------------------------------------
 323032332D30362D303100000000000000000000
(1 row)

SELECT HEX(WEIGHT_STRING('2023-06-01 00:23:59'::datetime as binary (20)));
                   hex                    
------------------------------------------
 323032332D30362D30312030303A32333A353900
(1 row)

SELECT HEX(WEIGHT_STRING('2023-06-01 00:23:59'::time as binary (20)));
                   hex                    
------------------------------------------
 30303A32333A3539000000000000000000000000
(1 row)

SELECT HEX(WEIGHT_STRING('2023-06-01 00:23:59'::timestamp as binary (20)));
                   hex                    
------------------------------------------
 323032332D30362D30312030303A32333A35392D
(1 row)

SELECT HEX(WEIGHT_STRING('2021-11-4 16:30:44.3411'::timestamptz as binary (20)));
                   hex                    
------------------------------------------
 323032312D31312D30342031363A33303A34342E
(1 row)

SELECT HEX(WEIGHT_STRING('59 minute'::interval as binary (20)));
                   hex                    
------------------------------------------
 40203539206D696E730000000000000000000000
(1 row)

SELECT HEX(WEIGHT_STRING(date '2021-12-31' as binary (10)));
         hex          
----------------------
 323032312D31322D3331
(1 row)

select HEX(WEIGHT_STRING(b'101'));
 hex 
-----
 05
(1 row)

select HEX(WEIGHT_STRING(b'101' as char(10)));
         hex          
----------------------
 05000000000000000000
(1 row)

select HEX(WEIGHT_STRING(b'101' as char(10) level 1 desc));
         hex          
----------------------
 FAFFFFFFFFFFFFFFFFFF
(1 row)

select HEX(WEIGHT_STRING(b'101' as char(10) level 1 desc reverse));
         hex          
----------------------
 FFFFFFFFFFFFFFFFFFFA
(1 row)

select HEX(WEIGHT_STRING(b'101' as binary(10)));
         hex          
----------------------
 05000000000000000000
(1 row)

-- 变量测试
SET enable_set_variable_b_format = 1;
SET @var1 = 100;
SET @var2 = 'test';
SET @var3 = date '2021-12-31';
SELECT HEX(WEIGHT_STRING(@var1 as char(10))); 
 hex 
-----
 
(1 row)

SELECT HEX(WEIGHT_STRING(@var2 as char(10)));
                   hex                    
------------------------------------------
 0054004500530054002000200020002000200020
(1 row)

SELECT HEX(WEIGHT_STRING(@var3 as char(10)));
                   hex                    
------------------------------------------
 0032003000320031002D00310032002D00330031
(1 row)

SELECT HEX(WEIGHT_STRING(@var1 as BINARY (10))); 
 hex 
-----
 
(1 row)

SELECT HEX(WEIGHT_STRING(@var2 as BINARY (10)));
         hex          
----------------------
 74657374000000000000
(1 row)

SELECT HEX(WEIGHT_STRING(@var3 as BINARY (10)));
         hex          
----------------------
 323032312D31322D3331
(1 row)

SET @var4 = WEIGHT_STRING('test' AS BINARY(10));
SET @var5 = WEIGHT_STRING('test' AS char(10));
-- NULL值处理
select hex(weight_string(NULL));
 hex 
-----
 
(1 row)

select hex(weight_string(NULL as binary(10)));
 hex 
-----
 
(1 row)

select hex(weight_string(NULL as char(10)));
 hex 
-----
 
(1 row)

select hex(weight_string('test' as char(NULL)));
ERROR:  syntax error at or near "NULL"
LINE 1: select hex(weight_string('test' as char(NULL)));
                                                ^
select hex(weight_string(''));
 hex 
-----
 
(1 row)

select hex(weight_string(' '));
 hex  
------
 0020
(1 row)

-- 超长字符串测试
select hex(weight_string(RPAD('a', 1000000, 'a') as char(10)));
                   hex                    
------------------------------------------
 0041004100410041004100410041004100410041
(1 row)

select hex(weight_string(RPAD('a', 1000000000, 'a') as char(1000000000))); 
WARNING:  requested length too large
CONTEXT:  referenced column: hex
 hex 
-----
 
(1 row)

select hex(weight_string(RPAD('a', 1000000000, 'a') as binary(1000000000))); 
WARNING:  requested length too large
CONTEXT:  referenced column: hex
 hex 
-----
 
(1 row)

-- 异常入参测试
select hex(weight_string('a' as char(-1)));  
ERROR:  syntax error at or near "-"
LINE 1: select hex(weight_string('a' as char(-1)));
                                             ^
select hex(weight_string('abc' as char(2) LEVEL 2-5 DESC));
ERROR:  syntax error at or near "DESC"
LINE 1: select hex(weight_string('abc' as char(2) LEVEL 2-5 DESC));
                                                            ^
SELECT HEX(WEIGHT_STRING('abc' as char(cast ( '10' as integer )) LEVEL '1'::numeric REVERSE));
ERROR:  syntax error at or near "cast"
LINE 1: SELECT HEX(WEIGHT_STRING('abc' as char(cast ( '10' as intege...
                                               ^
SELECT HEX(WEIGHT_STRING('ab' AS CHAR(1E+308)));
ERROR:  syntax error at or near "1E+308"
LINE 1: SELECT HEX(WEIGHT_STRING('ab' AS CHAR(1E+308)));
                                              ^
SELECT HEX(WEIGHT_STRING('ab' AS BINARY(1E+308)));
ERROR:  syntax error at or near "1E+308"
LINE 1: SELECT HEX(WEIGHT_STRING('ab' AS BINARY(1E+308)));
                                                ^
SELECT HEX(WEIGHT_STRING('ab' AS CHAR(1E+309)));
ERROR:  syntax error at or near "1E+309"
LINE 1: SELECT HEX(WEIGHT_STRING('ab' AS CHAR(1E+309)));
                                              ^
SELECT HEX(WEIGHT_STRING('ab' AS BINARY(1E+309)));
ERROR:  syntax error at or near "1E+309"
LINE 1: SELECT HEX(WEIGHT_STRING('ab' AS BINARY(1E+309)));
                                                ^
select hex(weight_string('a' as char(0)));
ERROR:  syntax error at or near 'char(0)'
select hex(weight_string('a' as binary(0)));
ERROR:  syntax error at or near 'binary(0)'
select hex(weight_string('a' as char(-1)));
ERROR:  syntax error at or near "-"
LINE 1: select hex(weight_string('a' as char(-1)));
                                             ^
select hex(weight_string('a' as binary(-1))); 
ERROR:  syntax error at or near "-"
LINE 1: select hex(weight_string('a' as binary(-1)));
                                               ^
SELECT HEX(WEIGHT_STRING('Hello' LEVEL -1)); 
ERROR:  syntax error at or near "-"
LINE 1: SELECT HEX(WEIGHT_STRING('Hello' LEVEL -1));
                                               ^
SELECT HEX(WEIGHT_STRING('Hello' LEVEL 7));
         hex          
----------------------
 00480045004C004C004F
(1 row)

SELECT HEX(WEIGHT_STRING('Hello' LEVEL 3,1));
         hex          
----------------------
 00480045004C004C004F
(1 row)

SELECT HEX(WEIGHT_STRING('Hello' LEVEL 3-1));
         hex          
----------------------
 00480045004C004C004F
(1 row)

-- 表达式
SELECT HEX(WEIGHT_STRING (UPPER('test') as char(10))); 
                   hex                    
------------------------------------------
 0054004500530054002000200020002000200020
(1 row)

SELECT HEX(WEIGHT_STRING (CONCAT('my', 'name') as char(10))); 
                   hex                    
------------------------------------------
 004D0059004E0041004D00450020002000200020
(1 row)

-- 兼容字符集测试（SQL_ASCII GBK LATIN1 UTF8）
SET client_encoding TO SQL_ASCII;
SELECT HEX(WEIGHT_STRING(CONVERT(E'\x57\x65\x69\x67\x68\x74\x20\x53\x74\x72\x69\x6e\x67\xe6\xb5\x8b\xe8\xaf\x95\x31' USING SQL_ASCII) AS CHAR(100))); 
                                                                                                                                                                                                       hex                                                                                                                                                                                                        
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 00570045004900470048005400200053005400520049004E00476D4B8BD50031002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020
(1 row)

SET client_encoding TO GBK;
SELECT HEX(WEIGHT_STRING(CONVERT(E'\x57\x65\x69\x67\x68\x74\x20\x53\x74\x72\x57\x65\x69\x67\x68\x74\x20\x53\x74\x72' USING GBK) AS CHAR(100))); 
                                                                                                                                                                                                       hex                                                                                                                                                                                                        
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 0057004500490047004800540020005300540052005700450049004700480054002000530054005200200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020
(1 row)

SET client_encoding TO LATIN1;
SELECT HEX(WEIGHT_STRING(CONVERT(E'\x57\x65\x69\x67\x68\x74\x20\x53\x74\x72\x57\x65\x69\x67\x68\x74\x20\x53\x74\x72' USING LATIN1) AS CHAR(100))); 
                                                                                                                                                                                                       hex                                                                                                                                                                                                        
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 0057004500490047004800540020005300540052005700450049004700480054002000530054005200200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020
(1 row)

SET client_encoding TO UTF8;
SELECT HEX(WEIGHT_STRING(CONVERT(E'\x57\x65\x69\x67\x68\x74\x20\x53\x74\x72\x69\x6e\x67\xe6\xb5\x8b\xe8\xaf\x95\x31' USING UTF8) AS CHAR(100))); 
                                                                                                                                                                                                       hex                                                                                                                                                                                                        
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 00570045004900470048005400200053005400520049004E00476D4B8BD50031002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020
(1 row)

-- 不兼容字符集测试（WIN874 LATIN 6 LATIN8 EUC_KR）
SET client_encoding TO WIN874;
SELECT WEIGHT_STRING(CONVERT('foo' USING WIN874) AS CHAR(100)) = WEIGHT_STRING(CONVERT('foo' USING LATIN6) AS CHAR(100));
 ?column? 
----------
 t
(1 row)

SELECT WEIGHT_STRING(CONVERT('foo' USING LATIN8) AS CHAR(100)) = WEIGHT_STRING(CONVERT('foo' USING WIN874) AS CHAR(100));
 ?column? 
----------
 t
(1 row)

SELECT WEIGHT_STRING(CONVERT('foo' USING EUC_KR) AS CHAR(100)) = WEIGHT_STRING(CONVERT('foo' USING LATIN8) AS CHAR(100));
 ?column? 
----------
 t
(1 row)

set enable_set_variable_b_format = default;
drop schema db_b_weightstring_test cascade;
reset current_schema;
